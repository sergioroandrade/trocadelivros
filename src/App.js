import React, { Component } from 'react'
import { Router, Route, Link, Switch } from 'react-router-dom'
//import Compare from './components/pages/compare/compare'
//import Cadastro from './components/pages/cadastro/cadastro'
import logo from './img/logoTemporario.png'
import IconHome from './components/icons/icon-home'
import IconList from './components/icons/icon-list'
import createBrowserHistory from 'history/createBrowserHistory'
import CadastrarLivro from './components/pages/livros/cadastrarLivro'
import MeusLivros from './components/pages/livros/meusLivros'
const customHistory = createBrowserHistory()

class App extends Component {
  render() {
    return (
      <div>
        <Router history={customHistory}>
          <div>
            <div className="header">
              <div className="content">
                <Link to="/">
                  <IconHome />
                </Link>
                <figure className="logo">
                  <img src={logo} alt="Trade Your Book logo" />
                </figure>
                <Link to="/compare">
                  <IconList />
                </Link>
              </div>
            </div>
            <Switch>
              <Route path="/index.html" component={MeusLivros} />
              <Route path="/" component={MeusLivros} />
              <Route path="/meusLivros" component={MeusLivros} />
              <Route path="/CadastrarLivro" component={CadastrarLivro} />
              <Route path="/CadastrarLivro/:id" component={CadastrarLivro} />
            </Switch>
          </div>
        </Router>
      </div>
    )
  }
}

export default App
