import {firebaseDatabase} from '../utils/firebaseUtils'

export default class FirebaseService {
    static getDataList = (nodePath, callback, size) => {

        let query = firebaseDatabase.ref(nodePath);

        if (size)
            query.limitToLast(size);

        query.on('value', dataSnapshot => {
            let items = [];
            dataSnapshot.forEach(childSnapshot => {
                let item = childSnapshot.val();
                item['key'] = childSnapshot.key;
                items.push(item);
            });
            callback(items);
        });

        return query;
    };

    static getUniqueDataBy = (id, node, callback) => {    
        let query = firebaseDatabase.ref(node + '/' + id);

        query.on('value', dataSnapshot => {
            let item = dataSnapshot.val();
            item['key'] = dataSnapshot.key;
            callback(item);
        });

        return query;
    }

    static pushData = (node, objToSubmit, callback) => {
        const ref = firebaseDatabase.ref(node).push();
        const id = firebaseDatabase.ref(node).push().key;
        ref.set(objToSubmit);

        if (callback) {
            callback(id);
        }

        return id;
    };

    static remove = (id, node) => {
        return firebaseDatabase.ref(node + '/' + id).remove();
    };

    static updateData = (id, node, objToSubmit, callback) => {
        firebaseDatabase.ref(node + '/' + id).update(objToSubmit);
        
        if (callback) {
            callback(id);
        }
    }
}