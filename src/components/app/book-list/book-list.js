import React, { Component } from 'react'
import './book-list.scss'

class BookList extends Component {
  
  constructor (props) {
    super(props)
    
    this.books = [
      {
        id:1,
        nome: "vrau1",
        autor: "vrau1",
        dataLancamento: Date.now(),
        tipoDeCapa: "Capa Comum",
        idioma: "Português"
      },
      {
        id:2,
        nome: "vraugen2",
        autor: "vraugen2",
        dataLancamento: Date.now(),
        tipoDeCapa: "Capa Grossa",
        idioma: "Inglês"
      }
    ]
  }

  renderBooks(book){
    return (
      <div className="Book-item-wrapper" data-index={book.id}>
        <div className="Book-item">
          <div className="Book-item__column">
            <strong className="item-brand">{book.nome}</strong>
            <span className="item-info">
              {book.autor} - {book.tipoDeCapa}
            </span>
          </div>
        </div>
      </div>
    )
  }

  render() {    
    return this.books.map(this.renderBooks)
  }
}
export default BookList
