import React, { Component } from 'react'
import FirebaseService from "../../services/firebaseService";
import './teste.scss'
import { Link } from 'react-router-dom'

class MeusLivros extends Component {

    constructor(props) {
        super(props)
 
        this.state = {
            livros: []
        }

        //this.handleInputChange = this.handleInputChange.bind(this)
        this.excluirLivro = this.excluirLivro.bind(this)
    }

    componentWillMount() {
        this.obterLivros(1);
    }
    
    obterLivros(usuarioId) {
        FirebaseService.getDataList("livros/", livros => {
            this.setState({ 
                livros : livros.filter(livro => { return livro.usuario === usuarioId })
            })
        })
    }

    excluirLivro(key){
        FirebaseService.remove(key, "livros");
    }

    render() {
        return (
        <div className="content">
            <h1>Meus Livros</h1>

            {
	            this.state.livros.map(livro =>{
                return (
                <div className='tile' key={livro.key}>
                    <img className='tile-img' src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ_sHkwqxAaTyH6EXe1qpINJF-2d1531fmoHFP4g0bsZvmAHPkK1g" alt="livro" />
                    <div className='tile-info'>
                        <h1>{livro.titulo}</h1>
                        <h2>{livro.autor}</h2>
                        <p>Páginas: {livro.paginas}</p>
                        <Link to={`/CadastrarLivro/${livro.key}`}>Editar</Link>
                         | 
                        <Link to="#" onClick={() => this.excluirLivro(livro.key)}>Excluir</Link>
                    </div>
                </div>
                )})
            }
        </div>
        )
    }
}

export default MeusLivros
