import * as firebase from 'firebase'

const config = {
    apiKey: "AIzaSyBV9o-RfWfRs5LfUCggNf-EOrlKXUYHnqM",
    authDomain: "trocadelivros-projetosocial.firebaseapp.com",
    databaseURL: "https://trocadelivros-projetosocial.firebaseio.com",
    projectId: "trocadelivros-projetosocial",
    storageBucket: "trocadelivros-projetosocial.appspot.com",
    messagingSenderId: "317177147829"
}

firebase.initializeApp(config)

export const firebaseDatabase = firebase.database()